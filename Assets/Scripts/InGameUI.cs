using UnityEngine;
using System.Collections;

public enum UIState {
	Normal,
	Paused,
}

public class InGameUI : MonoBehaviour {
	static public InGameUI main;
	public UIState state = UIState.Normal;

	public Banner winBanner;
	public Banner foreverWinBanner;
	public Banner instructionBanner;

	public Color[] winColors;

	public TextMesh[] lifeCounters;
	public Transform[] pointParents;
	public GameObject[] quadPrefabs;

	public GameObject[] winDeco;

	float pauseTimeout = 0f;

	void Awake(){
		main = this;
	}

	void Update(){
		pauseTimeout -= Time.deltaTime;

		for(int i = 0; i < 2; i++){
			var p = Manager.main.players[i];
			lifeCounters[i].text = p.lives.ToString();
		}

		switch(state){
			case UIState.Normal: {
				if(pauseTimeout <= 0f && Input.GetAxis("Pause") != 0f){
					Manager.main.Pause();
					state = UIState.Paused;
					pauseTimeout = 0.5f;
				}

				break;
			}

			case UIState.Paused: {
				if(pauseTimeout <= 0f && Input.GetAxis("Pause") != 0f){
					Manager.main.Resume();
					state = UIState.Normal;
					pauseTimeout = 0.5f;
				}

				break;
			}
		}
	}

	public void WinBanner(string text, int playerID){
		winBanner.Show(text, winColors[playerID]);
	}

	public void ForeverWinBanner(string text, int playerID){
		foreverWinBanner.Show(text, winColors[playerID]);
		winDeco[playerID].SetActive(true);
	}

	public void Instruct(string text){
		instructionBanner.Show(text, winColors[2]);
	}

	public void AddPoint(int playerID){
		var offset = new Vector3(0, quadPrefabs[0].transform.localScale.y*1.2f, 0);
		var go = (GameObject) Instantiate(quadPrefabs[playerID], pointParents[playerID].position - offset, Quaternion.identity);
		go.transform.parent = pointParents[playerID];
		pointParents[playerID] = go.transform;
	}

	void OnDrawGizmos(){
		// yuk
		Gizmos.DrawWireCube(transform.position + transform.forward*5f, new Vector3(5f*16f/9f, 5f, 5f)*2f);
	}
}
