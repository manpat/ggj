﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class boardGen : MonoBehaviour {

	private List<Block> blocksList;
	private Block[] blocks;
	public int numberOfQuads = 2;
	private const float maxScale = 4;

	void createGroup(Vector3 pos){
		blocksList = new List<Block> ();

		// Last type is cube which is not used alone
		//Block.typeBlock type = Block.typeBlock.LINE_H;
		Block.typeBlock type = (Block.typeBlock)(int)Mathf.Floor(Random.Range(1, 4));
		print ("Type: " + type.ToString ());
		
		if (type == Block.typeBlock.L_LEFT) {
			Block mainObj = new Block(type, (int)Mathf.Floor(Random.Range(1, maxScale)), pos);
			Vector3 fillingPos = new Vector3(pos.x + 0, 0, pos.z + 0);
			Block filling = new Block(Block.typeBlock.CUBE, (int)Mathf.Floor(Random.Range(1, maxScale)), fillingPos);
			print ("main: "+mainObj.ToString());
			blocksList.Add(mainObj);
			blocksList.Add(filling);
		}else if (type == Block.typeBlock.L_RIGHT) {
			print ("right");
			Block mainObj = new Block(type, (int)Mathf.Floor(Random.Range(1, maxScale)), pos);
			Vector3 fillingPos = new Vector3(pos.x + 0, 0, pos.z + 1);
			Block filling = new Block(Block.typeBlock.CUBE, (int)Mathf.Floor(Random.Range(1, maxScale)), fillingPos);
			print ("main: "+mainObj.ToString());
			blocksList.Add(mainObj);
			blocksList.Add(filling);
		}else if (type == Block.typeBlock.LINE_V) {
			Block mainObj = new Block(type, (int)Mathf.Floor(Random.Range(1, maxScale)), pos);
			Vector3 fillingPos = new Vector3(pos.x + 1, 0, pos.z + 0);
			Block filling = new Block(type, (int)Mathf.Floor(Random.Range(1, maxScale)), fillingPos);
			print ("main: "+mainObj.ToString());
			blocksList.Add(mainObj);
			blocksList.Add(filling);
		}else if (type == Block.typeBlock.LINE_H) {
			Block mainObj = new Block(type, (int)Mathf.Floor(Random.Range(1, maxScale)), pos);
			Vector3 fillingPos = new Vector3(pos.x + 0, 0, pos.z + 1);
			Block filling = new Block(type, (int)Mathf.Floor(Random.Range(1, maxScale)), fillingPos);
			print ("main: "+mainObj.ToString());
			blocksList.Add(mainObj);
			blocksList.Add(filling);
		}
	}

	void createBoard(){
		float quadWidth = 2f;
		float xPos = -numberOfQuads * 0.5f * quadWidth;
		float zPos = -numberOfQuads * 0.5f * quadWidth;
		float initXPos = xPos;

		//createGroup (new Vector3(0f, 0f, 0f));


		for (int i = 0; i < numberOfQuads; i++) {
			print ("");
			xPos = initXPos;
			for(int j = 0; j < numberOfQuads; j++){
				createGroup(new Vector3(xPos, 0f, zPos));
				xPos += quadWidth;
			}
			zPos += quadWidth;
		}
		blocks = new Block[blocksList.Count];
		int counter = 0;
		foreach (Block b in blocksList) {
			blocks[counter++] = b;
		}
	}

	boardGen(int numberOfQuads){
		this.numberOfQuads = numberOfQuads;
	}

	// Use this for initialization
	void Start () {
		createBoard ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
