﻿using UnityEngine;
using System.Collections;

public enum PlayerState {
	Normal,
	Shoved,
	Dodgeball,
}

public class Player : Pausable {
	public Transform spawnpoint;
	public Transform holdpoint;

	public int playerID = 0;
	public float speed = 5f;

	public int lives = 2;
	public float shoveRadius = 2f;

	public float stunTime = 1.5f;
	public float actionTimeout = 0.5f;
	public float actionAnimationTimeout = 0.25f;

	float stunCountdown = 0f;
	float actionCountdown = 0f;

	public bool isPushing = false;
	public bool isStunned = false;
	public bool isDead = false;

	public float lockDir = 0f;

	public Dodgeball dodgeball;

	public PlayerState state = PlayerState.Normal;

	// Rigidbody r; // this is inherited
	Transform t;
	Animator an;

	void Awake () {
		r = rigidbody;
		t = transform;
		an = GetComponent<Animator>();
	}
	
	void Update () {
		if(paused) return;

		actionCountdown -= Time.deltaTime;
		Vector3 vel = Vector3.zero;

		switch(state){
			case PlayerState.Normal:{
				vel = r.velocity;

				vel.x = Input.GetAxis("Horizontal" + playerID.ToString()) * speed;
				vel.z = Input.GetAxis("Vertical" + playerID.ToString())	* speed;

				if(actionCountdown < actionAnimationTimeout) isPushing = false;
			
				if(actionCountdown <= 0f && Input.GetAxis("Action" + playerID.ToString()) != 0f){
					isPushing = true;
					actionCountdown = actionTimeout;
					DoAction();
					actionAnimationTimeout = actionCountdown - 0.5f;
				}

				r.velocity = vel;
				break;
			}

			case PlayerState.Shoved: {
				stunCountdown -= Time.deltaTime;
				if(stunCountdown <= 0f){
					state = PlayerState.Normal;
					isStunned = false;
				}

				vel = r.velocity;
				vel *= 0.9f;
				r.velocity = vel;
				break;
			}

			case PlayerState.Dodgeball: {
				vel = r.velocity;

				vel.x = Input.GetAxis("Horizontal" + playerID.ToString()) * speed;
				vel.z = Input.GetAxis("Vertical" + playerID.ToString())	* speed;

				if(Input.GetAxis("Action" + playerID.ToString()) != 0f){
					dodgeball.Throw();
				}

				r.velocity = vel;
				break;
			}
		}

		DoAnimation(vel.x, vel.z);
	}

	void DiePls(){
		if(lives > 1) {
			lives--;
			Respawn();
		}else{
			lives = 0;
			Die();
		}
	}

	public void Respawn(){
		t.position = spawnpoint.position;
		animState = AnimationState.Idle;
		Manager.main.OnPlayerRespawn(playerID);
	}

	void Die(){
		if(isDead) return;

		isDead = true;
		Manager.main.OnPlayerDeath(playerID);
	}

	public void OnNewLevel(){
		DoAnimation(0f, 0f, true);
		isDead = false;
		isStunned = false;
		isPushing = false;
	}

	public void OnEndLevel(){
		lives = 2;
		lockDir = 0f;
		// animState = AnimationState.Idle;
		state = PlayerState.Normal;
	}

	void DoAction(){
		Collider[] cs = Physics.OverlapSphere(t.position, shoveRadius, LayerMask.GetMask("Player"));
		AudioManager.main.PlayChestBump();

		foreach(var c in cs){
			if(c != collider){
				var p = c.GetComponent<Player>();
				p.OnShoved();
				actionCountdown *= 3f;

				var pr = p.rigidbody;
				var diff = p.transform.position - t.position;
				diff.y = 0f;
				pr.velocity = diff.normalized * 1.6f / Time.deltaTime;
			}
		}
	}

	void OnCollisionEnter(Collision col){
		if(col.gameObject.CompareTag("Player")){
			Manager.main.OnPlayerCollide(playerID);
		}
	}

	public void OnShoved(){
		state = PlayerState.Shoved;
		stunCountdown = stunTime;
		isStunned = true;
		AudioManager.main.PlayPlayerHit();

		Manager.main.OnPlayerShoved(playerID);
	}

	enum AnimationState {
		Idle,
		RunLeft,
		RunRight,
		Shoved,
		Push,
	}

	private AnimationState animState = AnimationState.Idle;
	private float dirFacing = 1f;

	void DoAnimation(float xvel, float zvel, bool forceAnimationUpdate = false){
		bool dirty = forceAnimationUpdate;

		if(isStunned){
			if(animState != AnimationState.Shoved){
				dirty = true;
				animState = AnimationState.Shoved;
			}else{
				return;
			}

		}else if(isPushing && animState != AnimationState.Push){
			dirty = true;
			animState = AnimationState.Push;

		}else if(!isPushing){
			if(xvel == 0f){
				if(zvel > 0f && animState != AnimationState.RunRight){
					animState = AnimationState.RunRight;
					dirty = true;

				}else if(zvel < 0f && animState != AnimationState.RunLeft){
					animState = AnimationState.RunLeft;
					dirty = true;

				}else if(zvel == 0f && animState != AnimationState.Idle){
					animState = AnimationState.Idle;
					dirty = true;				
				}

			}else{
				if(xvel > 0f && animState != AnimationState.RunRight){
					dirty = true;
					animState = AnimationState.RunRight;

				}else if(xvel < 0f && animState != AnimationState.RunLeft){
					dirty = true;
					animState = AnimationState.RunLeft;
				}
			}
		}


		if(dirty){
			var pid = "player" + (playerID+1).ToString();
			switch(animState){
				case AnimationState.Idle:
					an.Play(pid + "idle");
					break;

				case AnimationState.Push:
					an.Play(pid + "push");
					break;

				case AnimationState.RunLeft:
					dirFacing = -1f;
					an.Play(pid + "run");
					break;

				case AnimationState.RunRight:
					dirFacing = 1f;
					an.Play(pid + "run");
					break;

				case AnimationState.Shoved:
					an.Play(pid + "knockback");
					break;
			}

			if(lockDir != 0f){
				dirFacing = lockDir;
			}

			t.localScale = new Vector3(dirFacing, 1, 1);
		}
	}

	public void Step(){
		AudioManager.main.PlayStep();
	}
}
