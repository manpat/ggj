using UnityEngine;
using System.Collections;

public class Banner : MonoBehaviour {
	public TextMesh text;

	public Transform shownPoint;
	public Transform hiddenPoint;

	public float timeVisible = 1.5f;

	float visibility = 0f;
	float visibilityTarget = 0f;
	float countdown;

	Transform t;

	void Awake(){
		t = transform;
	}

	public void Show(string _t, Color _c){
		text.text = _t;
		text.color = _c;
		visibilityTarget = 1f;

		countdown = timeVisible;
	}

	void Update(){
		countdown -= Time.deltaTime;
		if(countdown <= 0f){
			visibilityTarget = 0f;
		}

		visibility = Mathf.Lerp(visibility, visibilityTarget, Time.deltaTime*3f);
		t.position = Vector3.Lerp(hiddenPoint.position, shownPoint.position, visibility);
	}
}