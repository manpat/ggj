using UnityEngine;
using System.Collections;

public class Pausable : MonoBehaviour {
	public bool paused = false;

	Vector3 pauseVel;

	protected Rigidbody r;

	void Awake(){
		r = rigidbody;
	}

	void OnPauseGame(bool _p){
		paused = _p;
		if(_p){
			if(r){
				pauseVel = r.velocity;
				r.velocity = Vector3.zero;
			}
		}else{
			if(r){
				r.velocity = pauseVel;
			}			
		}
	}
}