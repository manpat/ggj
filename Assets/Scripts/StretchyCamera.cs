﻿using UnityEngine;
using System.Collections;

public enum CameraState {
	Stretching, // Normal
	Fixed,
}

[System.Serializable]
class CameraDimensions {
	public float margin;
	public float minSize;
	public float size;
	public Vector3 center;

	public CameraDimensions Clone(){
		var ret = new CameraDimensions();
		ret.margin = margin;
		ret.minSize = minSize;
		ret.size = size;
		ret.center = center;
		return ret;
	}
}

public class StretchyCamera : MonoBehaviour {
	static public StretchyCamera main;

	public CameraState state;
	[SerializeField] CameraDimensions dims;
	[SerializeField] CameraDimensions stretchDims;
	[SerializeField] CameraDimensions fixedDims;

	public Player p1;
	public Player p2;

	public float fixedCountdown = 0f;
	public float angle;
	public float distance;

	Camera c;
	Transform t;

	void Awake () {
		main = this;
		t = transform;
		c = GetComponent<Camera>();
		dims = stretchDims.Clone();
	}

	void Update () {
		InterpolateDimensions();

		switch(state){
			case CameraState.Stretching: {
				stretchDims.center = GetCenter();

				var dist = GetDistance();
				dist = Mathf.Max(dist, dims.minSize) + dims.margin;
				stretchDims.size = dist/2f;

				break;
			}

			case CameraState.Fixed: {
				fixedCountdown -= Time.deltaTime;
				if(fixedCountdown <= 0f){
					state = CameraState.Stretching;
				}

				break;
			}
		}

		t.rotation = Quaternion.Euler(angle,0,0);
		t.position = dims.center - t.forward * distance;
		c.orthographicSize = dims.size;
	}

	Vector3 GetCenter(){
		var t1 = p1.transform.position;
		var t2 = p2.transform.position;

		return (t1 + t2)/2f;
	}

	float GetDistance(){
		var t1 = p1.transform.position;
		var t2 = p2.transform.position;

		return (t2 - t1).magnitude;
	}

	void InterpolateDimensions(){
		CameraDimensions d = stretchDims;
		switch(state){
			case CameraState.Stretching: d = stretchDims; break;
			case CameraState.Fixed: d = fixedDims; break;
		}

		dims.margin = Mathf.Lerp(dims.margin, d.margin, Time.deltaTime*2f);
		dims.minSize = Mathf.Lerp(dims.minSize, d.minSize, Time.deltaTime*2f);

		dims.size = Mathf.Lerp(dims.size, d.size, Time.deltaTime*4f);
		dims.center = Vector3.Lerp(dims.center, d.center, Time.deltaTime*4f);
	}
}
