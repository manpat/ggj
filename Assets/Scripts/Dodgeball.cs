﻿using UnityEngine;
using System.Collections;

public enum DodgeballState {
	Idle,
	Thrown,
	Returning,
}

public class Dodgeball : Pausable {
	public DodgeballState state = DodgeballState.Idle;
	public float speed;
	public Player master;
	public float throwDir;

	bool hasHit = false;

	Transform t;

	void Awake(){
		r = rigidbody;
		t = transform;
	}

	void Update(){
		if(paused) return;

		switch(state){
			case DodgeballState.Idle: {
				t.position = master.holdpoint.position;
				hasHit = false;
				break;
			}

			case DodgeballState.Thrown: {
				r.velocity = Vector3.right * throwDir * speed;
				break;
			}

			case DodgeballState.Returning: {
				var diff = master.transform.position - t.position;
				r.velocity = diff.normalized * speed;
				break;
			}
		}
	}

	public void Throw(){
		if(state != DodgeballState.Idle) return;

		state = DodgeballState.Thrown;
		Detach();
		AudioManager.main.PlayThrowBall();
	}

	void OnTriggerEnter(Collider col){
		if(col.CompareTag("BallBoundary")){
			state = DodgeballState.Returning;
			AudioManager.main.PlayBallBounce();

		}else if(col.gameObject == master.gameObject){
			Attach();
			state = DodgeballState.Idle;

		}else if(!hasHit && col.CompareTag("Player")){
			col.SendMessage("DiePls");
			hasHit = true;
			state = DodgeballState.Returning;
			AudioManager.main.PlayBallBounce();

		}else if(col.CompareTag("Ball")){
			state = DodgeballState.Returning;
			AudioManager.main.PlayBallBounce();
		}
	}

	public void Attach(){
		t.parent = master.transform;
		r.velocity = Vector3.zero;
	}

	public void Detach(){
		t.parent = null;
		t.position = master.transform.position + master.transform.forward + master.transform.up * 0.75f;
	}
}
