﻿using UnityEngine;
using System.Collections;

public class Block {
	public enum typeBlock{
		L_RIGHT = 1,
		L_LEFT = 2,
		LINE_V = 3,
		LINE_H = 4,
		CUBE = 5
	}
	/*

		T_RIGHT = 3, 
		T_LEFT = 4,
	*/

	private GameObject[] cubes;
	private int nCubes = 0;

	public Block( typeBlock type, float scale, Vector3 pos){
		Debug.Log ("NEW B: T:"+type.ToString()+"  scale: "+scale+"   pos: "+pos);
		Vector3 scaleVector = new Vector3(1f, scale, 1f);
		float yPos = (scale - 1) * 0.5f;
		Debug.Log ("YPOS: "+yPos);
		if (type == typeBlock.L_LEFT) {
			cubes = new GameObject[3];
			cubes[0] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);	
			cubes[0].transform.localScale = scaleVector;
			cubes[0].transform.position = new Vector3(pos.x + 0f, yPos, pos.z + 1f);
			cubes[1] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);	
			cubes[1].transform.localScale = scaleVector;
			cubes[1].transform.position = new Vector3(pos.x + 1f, yPos, pos.z + 1f);
			cubes[2] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);
			cubes[2].transform.localScale = scaleVector;
			cubes[2].transform.position = new Vector3(pos.x + 1f, yPos, pos.z + 0f);
			nCubes = 3;
		}else if (type == typeBlock.L_RIGHT) {
			Debug.Log("right i");
			cubes = new GameObject[3];
			cubes[0] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);	
			cubes[0].transform.localScale = scaleVector;
			cubes[0].transform.position = new Vector3(pos.x + 0f, yPos, pos.z + 0f);
			cubes[1] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);	
			cubes[1].transform.localScale = scaleVector;
			cubes[1].transform.position = new Vector3(pos.x + 1f, yPos, pos.z + 0f);
			cubes[2] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);
			cubes[2].transform.localScale = scaleVector;
			cubes[2].transform.position = new Vector3(pos.x + 1f, yPos, pos.z + 1f);
			nCubes = 3;
		}else if (type == typeBlock.LINE_V) {
			cubes = new GameObject[2];
			cubes[0] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);
			cubes[0].transform.localScale = scaleVector;	
			cubes[0].transform.position = new Vector3(pos.x + 0f, yPos, pos.z + 0f);
			cubes[1] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);
			cubes[1].transform.localScale = scaleVector;	
			cubes[1].transform.position = new Vector3(pos.x + 0f, yPos, pos.z + 1f);
			nCubes = 2;
		}else if (type == typeBlock.LINE_H) {
			cubes = new GameObject[2];
			cubes[0] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);
			cubes[0].transform.localScale = scaleVector;	
			cubes[0].transform.position = new Vector3(pos.x + 0f, yPos, pos.z + 0f);
			cubes[1] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), Vector3.zero, Quaternion.identity);
			cubes[1].transform.localScale = scaleVector;	
			cubes[1].transform.position = new Vector3(pos.x + 1f, yPos, pos.z + 0f);
			nCubes = 2;
		}else if (type == typeBlock.CUBE) {
			Debug.Log("cube");
			cubes = new GameObject[1];
			pos.y = yPos;
			cubes[0] = (GameObject)MonoBehaviour.Instantiate(Resources.Load("cube"), pos, Quaternion.identity);
			// Uses the passed position
			cubes[0].transform.localScale = scaleVector;
			nCubes = 1;
		}
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < nCubes; i++) {
			cubes[i].transform.position = cubes[i].transform.position;	
		}
	}
}
