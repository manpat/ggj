﻿using UnityEngine;
using System.Collections;

public class KingHillLevel : Level {
	private float scaleHill = 30;
	private float scaleTime = 1f;

	public GameObject hill;
	
	public override void OnStart(){
		InGameUI.main.Instruct("Stay on the blue spot");
		
		foreach(var p in Manager.main.players){
			p.state = PlayerState.Normal;
		}

		scaleHill = 30;
		scaleTime = (scaleHill-3f)/Manager.main.levelTimeout;
	}
	
	public override void OnEnd(){
	}
	
	public override void OnUpdate(){
		if(!PlayerOnHill() || hill == null) return;

		scaleHill -= Time.deltaTime * scaleTime;
		var lsz = hill.transform.localScale.z;
		hill.transform.localScale = new Vector3(scaleHill, scaleHill, lsz);
	}
	
	public override void OnPlayerRespawn(Player pl){
	}
	
	public override void OnPlayerShoved(Player pl){
	}
	
	public override void OnPlayerCollide(Player pl){
	}
	
	public override void OnTimeout(){
		float p1dist = PlayerDistToCenter(0);
		float p2dist = PlayerDistToCenter(1);

		if(Mathf.Min(p1dist, p2dist) > scaleHill) {
			Manager.main.PlayerTie();

		}else{
			var whoone = (p1dist < p2dist) ? 0 : 1;
			Manager.main.PlayerWin (Manager.main.players[whoone].playerID);
		}

	}
	
	public void OnPlayerGetCrown(Player pl){
	}
	
	private float PlayerDistToCenter(int pl){
		var center = hill.transform.position;
		var plpos = Manager.main.players[pl].transform.position;
		center.y = 0;
		plpos.y = 0;

		return Vector3.Distance(center, plpos);
	}

	private bool PlayerOnHill(){
		var p1dist = PlayerDistToCenter(0);
		var p2dist = PlayerDistToCenter(1);

		var min = Mathf.Min(p1dist, p2dist);
		return min < scaleHill/2f;
	}
}
