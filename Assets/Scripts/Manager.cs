﻿using UnityEngine;
using System.Collections;

public enum LockLevel {
	DontLock = -1,
	Dodgeball = 0,
	Crown,
	Lazer,
	KingOfHill
}

public class Manager : Pausable {
	static public Manager main;

	public StretchyCamera stretchyCamera;

	public Player[] players;
	public float countdown;

	public GameObject[] levels;
	public Level currentLevel;

	public Transform levelSpawnpoint;

	public float levelTimeout = 12.5f;
	public float levelEndPause = 1f;
	public float winTimeout = 5f;

	public LockLevel lockLevel = LockLevel.DontLock;

	float levelCountdown = 0f;
	float levelWait = 0f;
	float winCountdown = Mathf.Infinity;

	int[] playerpoints;

	void Awake () {
		main = this;
	}

	void Start(){
		playerpoints = new int[2];
		players = new Player[2];
		foreach(var p in FindObjectsOfType<Player>()){
			players[p.playerID] = p;
		}

		NewLevel();
	}

	void Update(){
		winCountdown -= Time.deltaTime;
		if(winCountdown <= 0f){
			Application.LoadLevel("TitleScreen");
		}

		if(paused) return;
		if(levelCountdown > 0f && currentLevel) currentLevel.OnUpdate();

		levelCountdown -= Time.deltaTime;
		levelWait -= Time.deltaTime;
		if(levelCountdown <= 0f){
			DoTimeout();
			levelCountdown = Mathf.Infinity;
		}

		if(levelWait <= 0f){
			print("LevelWaited");
			NewLevel();
			levelWait = Mathf.Infinity;
		}
	}

	public void OnPlayerDeath(int pid){
		print("OnPlayerDeath");

		if(levelWait > levelEndPause){
			if(currentLevel) currentLevel.OnEnd();
			levelWait = levelEndPause;
			levelCountdown = Mathf.Infinity;
		}
	}

	public void OnPlayerRespawn(int pid){
		if(currentLevel) currentLevel.OnPlayerRespawn(players[pid]);
	}

	public void OnPlayerShoved(int pid){
		if(currentLevel) currentLevel.OnPlayerShoved(players[pid]);
	}

	public void OnPlayerCollide(int pid){
		if(currentLevel) currentLevel.OnPlayerCollide(players[pid]);
	}

	public void NewLevel(){
		int level = Random.Range(0, levels.Length);
		if(lockLevel != LockLevel.DontLock){
			level = (int) lockLevel;
		}

		var go = (GameObject)Instantiate(levels[level], levelSpawnpoint.position, Quaternion.identity);
		var lvl = go.GetComponent<Level>();

		if(currentLevel) {
			foreach(var p in players){
				p.OnEndLevel();
			}

			Destroy(currentLevel.gameObject);
		}

		currentLevel = lvl;

		for(int i = 0; i < 2; i++){
			players[i].spawnpoint = lvl.playerSpawnpoints[i];
		}

		lvl.OnStart();
		lvl.hasStarted = true;

		foreach(var p in players){
			p.Respawn();
			p.OnNewLevel();
		}

		levelCountdown = levelTimeout;
		levelWait = Mathf.Infinity;
	}

	void DoTimeout(){
		if(currentLevel) currentLevel.OnTimeout();
		if(currentLevel) currentLevel.OnEnd();
		levelWait = levelEndPause;
	}

	public void Pause(){
		Object[] objects = FindObjectsOfType (typeof(GameObject));
		foreach (GameObject go in objects) {
			go.SendMessage ("OnPauseGame", true, SendMessageOptions.DontRequireReceiver);
		}
		AudioManager.main.PlayPause();
	}

	public void Resume(){
		Object[] objects = FindObjectsOfType (typeof(GameObject));
		foreach (GameObject go in objects) {
			go.SendMessage ("OnPauseGame", false, SendMessageOptions.DontRequireReceiver);
		}
		AudioManager.main.PlayPause();
	}

	public void PlayerWin(int playerID){
		InGameUI.main.AddPoint(playerID);
		playerpoints[playerID]++;

		if(playerpoints[playerID] >= 5){
			string s = "red";
			if(playerID == 1) s = "blue";

			InGameUI.main.ForeverWinBanner(s + " player is manliest", playerID);
			winCountdown = winTimeout;
			AudioManager.main.PlayMoreApplause();
			Pause();
		}else{
			if(playerID == 0){
				InGameUI.main.WinBanner("Red Player Wins", 0);
			}else{
				InGameUI.main.WinBanner("Blue Player Wins", 1);
			}
		}

		AudioManager.main.PlayApplause();
	}

	public void PlayerTie(){
		InGameUI.main.WinBanner("No-one wins", 2);
	}
}
