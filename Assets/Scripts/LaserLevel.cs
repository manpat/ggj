﻿using UnityEngine;
using System.Collections;

public class LaserLevel : Level {
	public GameObject[] laserPrefabs;
	public Transform[] laserSpawnPoints;
	public float laserTimeoutOnPlayerRespawn = 1.5f;

	public Laser[] lasers;

	public override void OnStart(){
		lasers = new Laser[2];

		for(int i = 0; i < 2; i++){
			var g = (GameObject)Instantiate(laserPrefabs[i], laserSpawnPoints[i].position + Vector3.up*laserPrefabs[i].transform.localScale.y/2f, Quaternion.identity);
			lasers[i] = g.GetComponent<Laser>();
			lasers[i].target = Manager.main.players[i];
		}

		InGameUI.main.Instruct("Ahhh! Lasers!");
	}

	public override void OnUpdate(){

	}

	public override void OnEnd(){
		foreach(var l in lasers){
			if(l) Destroy(l.gameObject);
		}

		print("Laser OnEnd");
		
		var p1 = Manager.main.players[0];
		var p2 = Manager.main.players[1];

		if(p1.lives == p2.lives){
			Manager.main.PlayerTie();

		}else if(p1.lives > p2.lives){
			Manager.main.PlayerWin(0);

		}else{
			Manager.main.PlayerWin(1);
		}
	}

	public override void OnPlayerShoved(Player pl){

	}
	
	public override void OnPlayerCollide(Player pl){

	}

	public override void OnPlayerRespawn(Player pl){
		if(!hasStarted) return;

		lasers[pl.playerID].state = LaserState.LowPower;
		lasers[pl.playerID].countdown = laserTimeoutOnPlayerRespawn;
	}

	public override void OnTimeout(){
	}
}
