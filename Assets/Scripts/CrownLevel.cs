using UnityEngine;
using System.Collections;

public class CrownLevel : Level {
	public GameObject crownPrefab;

	public Crown crown;
	public bool[] playerHasCrown;

	float countdown = 0f;

	public override void OnStart(){
		var go = (GameObject) Instantiate(crownPrefab, Vector3.zero + Vector3.up, Quaternion.identity);
		crown = go.GetComponent<Crown>();

		playerHasCrown = new bool[2];

		InGameUI.main.Instruct("Become King");
	}

	public override void OnEnd(){
		if(crown != null){
			Destroy(crown.gameObject);
		}
	}

	public override void OnUpdate(){
		countdown -= Time.deltaTime;
	}

	public override void OnPlayerRespawn(Player pl){
		if(playerHasCrown[pl.playerID]){
			playerHasCrown[pl.playerID] = false;
			crown.canBeGot = true;
			crown.transform.parent = null;

			var p = crown.transform.position;
			p.y = 0.5f;
			crown.transform.position = p;
		}
	}

	public override void OnPlayerShoved(Player pl){
		OnPlayerRespawn(pl);
	}

	public override void OnPlayerCollide(Player pl){
		if(countdown <= 0f && playerHasCrown[pl.playerID]){
			playerHasCrown[pl.playerID] = false;
			playerHasCrown[1-pl.playerID] = true;
			AudioManager.main.PlayCollect();

			Player otherpl = Manager.main.players[1-pl.playerID];
			crown.transform.parent = otherpl.transform;
			crown.transform.position = otherpl.holdpoint.position;
			countdown = 0.5f;
			AudioManager.main.PlayBallBounce();
		}
	}

	public override void OnTimeout(){
		if(playerHasCrown[0]){
			Manager.main.PlayerWin(0);
		}else if(playerHasCrown[1]){
			Manager.main.PlayerWin(1);
		}else{
			Manager.main.PlayerTie();
		}
	}

	public void OnPlayerGetCrown(Player pl){
		playerHasCrown[pl.playerID] = true;
		crown.transform.parent = pl.transform;
		crown.transform.position = pl.holdpoint.position;
		AudioManager.main.PlayCollect();
	}
}