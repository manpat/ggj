﻿using UnityEngine;
using System.Collections;

public abstract class Level : MonoBehaviour {
	public Transform[] playerSpawnpoints;
	public bool hasStarted = false;

	public abstract void OnStart(); // Called when level reaches it's target (i.e when it's ready)
	public abstract void OnEnd(); // Called before level destruction
	public abstract void OnUpdate(); // Guess
	public abstract void OnPlayerRespawn(Player pl); // Called when player respawns
	public abstract void OnPlayerShoved(Player pl); // Called when player is shoved
	public abstract void OnPlayerCollide(Player pl); // Called when player collides with another
	public abstract void OnTimeout(); // Called when the level times out

	void Awake(){
		hasStarted = false;
	}
}
