using UnityEngine;
using System.Collections;

public class Crown : MonoBehaviour {
	public GameObject pixelLight;
	public bool canBeGot = true;

	void OnTriggerEnter(Collider col){
		if(canBeGot && col.CompareTag("Player")){
			var pl = col.GetComponent<Player>();
			if(pl.isStunned) return;

			var l = (CrownLevel)Manager.main.currentLevel;
			l.OnPlayerGetCrown(pl);
			canBeGot = false;
		}
	}

	void Update(){
		if(canBeGot){
			pixelLight.SetActive(true);
		}else{
			pixelLight.SetActive(false);
		}
	}
}