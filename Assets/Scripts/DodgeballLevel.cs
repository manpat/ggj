using UnityEngine;
using System.Collections;

public class DodgeballLevel : Level {
	public GameObject[] ballPrefabs;
	public Dodgeball[] dodgeballs;

	public override void OnStart(){
		dodgeballs = new Dodgeball[2];

		for(int i = 0; i < 2; i++){
			var p = Manager.main.players[i];

			p.state = PlayerState.Dodgeball;
			var go = (GameObject) Instantiate(ballPrefabs[i], Vector3.zero + Vector3.up * 4f, Quaternion.identity);
			var db = go.GetComponent<Dodgeball>();
			db.state = DodgeballState.Idle;
			p.dodgeball = db;

			float dir = 1f - i*2f;
			db.throwDir = dir;
			p.lockDir = dir;
			db.master = p;
			db.Attach();

			dodgeballs[i] = db;
		}

		print("Dodgeball OnStart");

		var sc = Manager.main.stretchyCamera;
		sc.state = CameraState.Fixed;
		sc.fixedCountdown = Mathf.Infinity;

		InGameUI.main.Instruct("Dodge ball");
	}

	public override void OnEnd(){
		foreach(var db in dodgeballs){
			if(db) Destroy(db.gameObject);
		}

		print("Dodgeball OnEnd");

		Manager.main.stretchyCamera.fixedCountdown = 0f;
		
		var p1 = Manager.main.players[0];
		var p2 = Manager.main.players[1];

		if(p1.lives == p2.lives){
			Manager.main.PlayerTie();

		}else if(p1.lives > p2.lives){
			Manager.main.PlayerWin(0);

		}else{
			Manager.main.PlayerWin(1);
		}
	}

	public override void OnUpdate(){

	}

	public override void OnPlayerRespawn(Player pl){

	}

	public override void OnPlayerShoved(Player pl){

	}

	public override void OnPlayerCollide(Player pl){

	}

	public override void OnTimeout(){

	}
}