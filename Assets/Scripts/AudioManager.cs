﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {
	static public AudioManager main;

	public AudioClip chestBump;
	public AudioClip playerHit;
	public AudioClip playerScream;
	public AudioClip ballBounce;
	public AudioClip applause;
	public AudioClip moreApplause;
	public AudioClip throwBall;
	public AudioClip pause;
	public AudioClip step;
	public AudioClip collect;

	void Awake () {
		main = this;
	}
	
	public void PlayChestBump(){
		AudioSource.PlayClipAtPoint(chestBump, Vector3.zero);
	}

	public void PlayPlayerHit(){
		AudioSource.PlayClipAtPoint(playerHit, Vector3.zero);
	}

	public void PlayPlayerScream(){
		AudioSource.PlayClipAtPoint(playerScream, Vector3.zero);
	}

	public void PlayBallBounce(){
		AudioSource.PlayClipAtPoint(ballBounce, Vector3.zero);
	}

	public void PlayApplause(){
		AudioSource.PlayClipAtPoint(applause, Vector3.zero);
	}

	public void PlayMoreApplause(){
		AudioSource.PlayClipAtPoint(moreApplause, Vector3.zero);
	}

	public void PlayThrowBall(){
		AudioSource.PlayClipAtPoint(throwBall, Vector3.zero);
	}

	public void PlayPause(){
		AudioSource.PlayClipAtPoint(pause, Vector3.zero);
	}

	public void PlayStep(){
		AudioSource.PlayClipAtPoint(step, Vector3.zero);
	}

	public void PlayCollect(){
		AudioSource.PlayClipAtPoint(collect, Vector3.zero);
	}
}
