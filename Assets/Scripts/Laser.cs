﻿using UnityEngine;
using System.Collections;

public enum LaserState {
	Start,
	Chase,
	LowPower,
}

public class Laser : Pausable {
	public Player target;

	public float countdown = 2f;
	public LaserState state = LaserState.Start;
	public float speed = 5f;

	Transform t;
	Transform pt;

	bool canKill = true;

	void Start () {
		t = transform;
		pt = target.transform;

		r = rigidbody;
	}
	
	void Update () {
		if(paused) return;

		switch(state){
			case LaserState.Start:{
				countdown -= Time.deltaTime;
				if(countdown <= 0f) state = LaserState.Chase;

				r.velocity = Vector3.zero;
				canKill = true;

				break;
			}

			case LaserState.Chase:{
				var diff = pt.position - t.position;
				diff.y = 0f;
				var vel = diff.normalized * speed;
				r.velocity = vel;
				canKill = true;

				break;
			}

			case LaserState.LowPower:{
				countdown -= Time.deltaTime;
				if(countdown <= 0f) state = LaserState.Chase;

				r.velocity = Vector3.zero;
				canKill = false;

				break;
			}
		}
	}

	void OnTriggerStay(Collider col){
		if(!paused && canKill && col.CompareTag("Player")){
			col.SendMessage("DiePls");
		}
	}
}
